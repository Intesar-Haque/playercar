package crud.myFrstCrud.service;

import crud.myFrstCrud.model.Player;
import crud.myFrstCrud.model.dto.PlayerAddReqDto;

public interface PlayerService {
    Player create(PlayerAddReqDto playerAddReqDto);
    Player delete(Long id);
    Player update(Long id, String color);
    Player addCar(Long playerId, Long carId);
    Player fetch(Long playerId);

}
