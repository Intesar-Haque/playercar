package crud.myFrstCrud.service;

import crud.myFrstCrud.model.Car;
import crud.myFrstCrud.model.dto.CarAddReqDto;

public interface CarService {
    Car create(CarAddReqDto carAddReqDto);
    Car delete(Long id);
    Car update(Long id, String contactNo);
}
