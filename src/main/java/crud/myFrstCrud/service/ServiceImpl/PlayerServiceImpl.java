package crud.myFrstCrud.service.ServiceImpl;

import crud.myFrstCrud.model.Car;
import crud.myFrstCrud.model.Player;
import crud.myFrstCrud.model.dto.PlayerAddReqDto;
import crud.myFrstCrud.repo.CarRepo;
import crud.myFrstCrud.repo.PlayerRepo;
import crud.myFrstCrud.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    PlayerRepo playerRepo;
    @Autowired
    CarRepo carRepo;
    EntityManager entityManager;
    @Override
    public Player create(PlayerAddReqDto playerAddReqDto) {
        Player player = new Player();
        player.setName(playerAddReqDto.getName());
        player.setContactNo(playerAddReqDto.getContactNo());
        return playerRepo.save(player);
    }

    @Override
    public Player delete(Long id) {
        Player player = playerRepo.findById(id);
        List<Car> newList = new ArrayList<>();
        player.setCars(newList);
        playerRepo.save(player);
        playerRepo.deleteById(id);
        return null;
    }

    @Override
    public Player update(Long id, String contactNo) {
        Player player = playerRepo.findById(id);
        player.setContactNo(contactNo);
        return playerRepo.save(player);
    }

    @Override
    public Player addCar(Long playerId, Long carId) {
        Player player = playerRepo.findById(playerId);
        Car car = carRepo.findById(carId);
        List<Car> carList = player.getCars();
        carList.add(car);
        player.setCars(carList);
        return playerRepo.save(player);
    }

    @Override
    public Player fetch(Long id) {
        Player player = playerRepo.findById(id);
//      Car car = carRepo.findById(id);
        return player;
    }
}
