package crud.myFrstCrud.service.ServiceImpl;

import crud.myFrstCrud.model.Car;
import crud.myFrstCrud.model.dto.CarAddReqDto;
import crud.myFrstCrud.repo.CarRepo;
import crud.myFrstCrud.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CarServiceImpl implements CarService {
    @Autowired
    CarRepo carRepo;

    @Override
    public Car create(CarAddReqDto carAddReqDto) {
        Car car = new Car();
        car.setColor(carAddReqDto.getColor());
        car.setModelNo(carAddReqDto.getModelNo());
        car.setName(carAddReqDto.getName());
        car.setPrice(carAddReqDto.getPrice());
        return carRepo.save(car);
    }

    @Override
    public Car delete(Long id) {
        carRepo.deleteById(id);
        return null;
    }

    @Override
    public Car update(Long id, String color) {
        Car car = carRepo.findById(id);
        car.setColor(color);
        return carRepo.save(car);

    }

}
