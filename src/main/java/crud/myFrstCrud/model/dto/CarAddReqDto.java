package crud.myFrstCrud.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarAddReqDto {
    String name;
    Integer price;
    String modelNo;
    String color;
}
