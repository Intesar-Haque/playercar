package crud.myFrstCrud.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlayerAddReqDto {
    String name;
    String contactNo;
}
