package crud.myFrstCrud.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name ="Player")
public class Player {
    @ManyToMany( fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "players_car",
            joinColumns = { @JoinColumn(name = "playerId") },
            inverseJoinColumns = { @JoinColumn(name = "carId") }
    )
    List<Car> cars = new ArrayList<>();
    @Id
    @GeneratedValue
    Long id;
    @NotNull
    String name;
    String contactNo;

}
