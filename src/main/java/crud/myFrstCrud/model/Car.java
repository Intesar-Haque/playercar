package crud.myFrstCrud.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@Entity
@Table(name ="Car")
public class Car {
    @JsonIgnore
    @ManyToMany( fetch = FetchType.LAZY, mappedBy = "cars")
    private List<Player> players = new ArrayList<>();
    @Id
    @GeneratedValue
    Long id;
    @NotNull
    String name;
    String modelNo;
    String color;
    Integer price;
}
