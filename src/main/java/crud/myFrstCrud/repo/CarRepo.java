package crud.myFrstCrud.repo;

import crud.myFrstCrud.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface CarRepo extends JpaRepository<Car,String> {

    List<Car> findByNameAndColor(String name, String  color);
    Car findById(Long id);


    @Transactional
    @Modifying
    void deleteAllByName(String name);

    @Transactional
    @Modifying
    void deleteById(Long id);




}
