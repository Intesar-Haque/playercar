package crud.myFrstCrud.repo;

import crud.myFrstCrud.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface PlayerRepo extends JpaRepository<Player,String> {
    Player findById(Long id);
    @Transactional
    @Modifying
    void deleteById(Long id);
}
