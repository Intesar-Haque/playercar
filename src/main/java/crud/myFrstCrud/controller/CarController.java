package crud.myFrstCrud.controller;

import crud.myFrstCrud.model.Car;
import crud.myFrstCrud.model.dto.CarAddReqDto;
import crud.myFrstCrud.service.CarService;
import crud.myFrstCrud.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController()
public class CarController {
    @Autowired
    private CarService carService;
    @Autowired
    private PlayerService playerService;
    @RequestMapping("/car/create")
    public Car carCreate(@RequestBody CarAddReqDto carAddReqDto) {
        return carService.create(carAddReqDto);
    }
    @RequestMapping("/car/update")
    public Car carUpdate(@RequestParam Long id, @RequestParam String color) {
        return carService.update(id, color);
    }
    @GetMapping("/car/delete")
    public Car carDelete(@RequestParam Long id) {
        return carService.delete(id);
    }
}
