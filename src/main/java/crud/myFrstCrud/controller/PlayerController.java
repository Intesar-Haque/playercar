package crud.myFrstCrud.controller;

import crud.myFrstCrud.model.Player;
import crud.myFrstCrud.model.dto.PlayerAddReqDto;
import crud.myFrstCrud.service.CarService;
import crud.myFrstCrud.service.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class PlayerController {
//    Logger logger = LoggerFactory.getLogger(PlayerController.class);
    @Autowired
    private PlayerService playerService;

    @RequestMapping("/assign")
    public Player addCar(@RequestParam Long carId, @RequestParam Long playerId) {
        return playerService.addCar(playerId, carId);
    }
    @RequestMapping("/fetch")
    public Player fetch(@RequestParam Long playerId) {

        return playerService.fetch(playerId);
    }

    @RequestMapping("/player/create")
    public Player playerCreate(@RequestBody PlayerAddReqDto playerAddReqDto) {
        return playerService.create(playerAddReqDto);
    }
    @RequestMapping("/player/update")
    public Player playerUpdate(@RequestParam Long id, @RequestParam String contactNo) {
        return playerService.update(id, contactNo);
    }
    @GetMapping("/player/delete")
    public Player playerDelete(@RequestParam Long id) {
        return playerService.delete(id);
    }

}
